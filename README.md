# chopfile

Create a copy of a file, truncated by N lines.

### Usage

To create a of a file that ignores (chops off) 150 lines, run

```
chopfile -input somefile.txt -chop 150
```

A new file **somefile.txt_truncated** will be created. The original file will be
preserved.

### Why?

Sometimes you just need to ignore the first N lines of a file. This is actually
a little tricky in unix, since it isn't possible to "move" the head/first byte
of a file forward safely without a copy. I needed this for log files, and
all the recommendations I saw on the internet was some horrid `sed` incantation.

