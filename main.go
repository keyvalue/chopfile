package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
)

var (
	input = flag.String("input", "", "input file; a \"_truncated\" version will be generated (or overwritten if it exists); original -input file is preserved")
	chop  = flag.Int("chop", 0, "lines to remove from beginning; e.g. -chop 10 will copy to a truncated file everything after 10 lines")
)

func main() {
	flag.Parse()
	if *chop == 0 {
		log.Fatal("you must provide a value for -chop and -input flags")
	}

	infile, err := os.Open(*input)
	if err != nil {
		log.Fatalf("could not open %s: %v", *input, err)
	}
	outfile, err := os.Create(fmt.Sprintf("%s_truncated", *input))
	if err != nil {
		log.Fatalf("could not open outfile: %v", err)
	}

	scnr := bufio.NewScanner(infile)

	linecount := 0
	for scnr.Scan() {
		linecount++
		if linecount > *chop {
			line := scnr.Text()
			outfile.WriteString(line + "\n")
		}
	}
}
